﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//The action class derives from scriptableObject, because it doesn't need access to MonoBehaviour callbacks like Update()
public class Action : ScriptableObject
{
    public string actionName;

    protected virtual void PerformAction(GameObject playerGO)
    {
        Debug.Log("Performing action: " + actionName);
    }
}
